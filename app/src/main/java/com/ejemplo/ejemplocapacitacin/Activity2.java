package com.ejemplo.ejemplocapacitacin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class Activity2 extends AppCompatActivity {
    private ListView listView;
    private ArrayAdapter<String> listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        listView = findViewById(R.id.listView);


        ArrayList<String> items = new ArrayList<>();
        items.add("celda 1");
        items.add("celda 2");

        listAdapter = new ArrayAdapter<>(Activity2.this, android.R.layout.simple_list_item_1);
        listAdapter.addAll(items);
        listView.setAdapter(listAdapter);
    }
}
